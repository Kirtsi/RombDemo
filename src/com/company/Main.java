package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String getWord;

        System.out.println("Enter the word you want to rhombify: ");
        getWord = br.readLine();

        if (getWord.matches("[a-zA-Z]+")) {
            rhombify(getWord.toLowerCase());
        } else {
            throw new IllegalArgumentException("Please enter ONLY letters");
        }
    }

    public static void rhombify(String x) {
        for (int i = 1; i <= x.length(); ++i) {
            System.out.println(x.substring(0, i));
        }
        for (int i = 1; i <= x.length(); ++i) {
            for (int y = i; y > 0; --y) {
                System.out.print(" ");
            }
            System.out.println(x.substring(i));
        }
    }
}
